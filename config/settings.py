from pathlib import Path
import yaml

default_file = Path(__file__).parent / 'config.yaml'

with open(default_file, 'r') as file:
    config = yaml.safe_load(file)

db_config = config.get('database')
csv_config = config.get('csv_config')
worker = config.get('worker')
