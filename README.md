### install dependencies

```bash
python3.7 -m venv ./venv
. ./venv/bin/activate
pip install -r ./requirements.txt
```

### configuration settings

```yaml
database:
  host: 'localhost'
  port: 3306
  database: 'csv_queue'
  user: 'csv_queue'
  password: 'ak3a18l'

csv_config:
  delimiter: ','
  data_file: './data.csv'

worker: './worker.php'
```

### database setup

```mysql
create database `csv_queue` /*!40100 collate 'utf8_unicode_ci'*/;
create user 'csv_queue'@'localhost' identified by 'ak3a18l';
grant all on csv_queue.* to 'csv_queue'@'localhost';
flush privileges;
```

### initialize tasks table

```mysql
CREATE TABLE IF NOT EXISTS tasks (
    id INT AUTO_INCREMENT,
    task_id INT NOT NULL,
    output TEXT DEFAULT NULL,
    PRIMARY KEY (id)
)  ENGINE=INNODB;
```