#!/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
import time
import MySQLdb
import subprocess
from typing import List
from config import db_config, csv_config, worker


def parse_data_file() -> List[int]:
    """
    parse csv-file
    :return:
    """
    with open(csv_config['data_file'], mode='r') as file:
        ids = []
        csv_reader = csv.DictReader(file, delimiter=csv_config['delimiter'])
        line_count = 0
        for row in csv_reader:
            ids.append(int(row['id']))
            line_count += 1
        return ids


def save_parsed_data(data=None) -> None:
    """
    save parsed csv data into database
    :param data:
    :return:
    """
    if data is None:
        return
    db = init_db()
    cur = db.cursor(MySQLdb.cursors.DictCursor)
    cur.execute("SELECT * FROM tasks")
    db_items = cur.fetchall()

    # exclude existed ids
    items_ids = [item['task_id'] for item in db_items]
    new_ids = list(set(data) - set(items_ids))

    cur.executemany("""INSERT INTO tasks (task_id) VALUES (%s)""", new_ids)
    db.commit()


def init_db() -> 'MySQLdb.Connection':
    """
    initialize db connection
    :return:
    """
    db = MySQLdb.connect(host=db_config['host'],
                         user=db_config['user'],
                         password=db_config['password'],
                         db=db_config['database'])
    return db


def process_data():
    """
    data processing
    :return:
    """
    db = init_db()
    cursor = db.cursor(MySQLdb.cursors.DictCursor)
    processing_results = []
    with cursor:
        items_processed = cursor.execute("SELECT * FROM tasks")
        for row in cursor.fetchall():
            result = subprocess.check_output([f"{worker}", f"{row['task_id']}"])
            processing_results.append((result.decode("utf-8"), row['id']))
    cursor = db.cursor()
    items_updated = cursor.executemany("UPDATE tasks SET output = %s WHERE id = %s ", processing_results)
    db.commit()
    return items_processed, items_updated


if __name__ == '__main__':
    start_time = time.time()
    ids = parse_data_file()
    save_parsed_data(data=ids)

    """ passing multiple params """
    # arr_to_worker = list([f"arr[]={id}&" for id in ids])
    # worker_param = ''.join(arr_to_worker)
    # subprocess.call([f"{worker}", f"{worker_param}"])
    # ----------------------------

    items_processed, items_updated = process_data()

    print("+"+"-"*24+"+")
    print(f"| worker: {worker}")
    print(f"| items processed: {items_processed}")
    print(f"| items updated: {items_updated}")
    print(f"| processing time: {round(time.time() - start_time, 2)} seconds")
    print("+" + "-" * 24 + "+")

